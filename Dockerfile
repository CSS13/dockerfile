FROM node:argon
MAINTAINER Emil 'Skeen' Madsen <sovende@gmail.com>
ENV DEBIAN_FRONTEND noninteractive

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install http-server globally
RUN npm install -g http-server

# Install app dependencies
# DHT-fake
RUN mkdir -p /usr/src/app/dht-fake/
COPY dht-fake/package.json dht-fake/
RUN cd dht-fake && npm install
# AngularJS project
RUN mkdir -p /usr/src/app/angular/
COPY angular/dist/ angular/

# Bundle app source
COPY dht-fake/ /usr/src/app/dht-fake

# Expose our port
EXPOSE 1987
EXPOSE 3000
EXPOSE 3001

# Include launch script
COPY run.sh /usr/src/app/
CMD [ "./run.sh" ]
