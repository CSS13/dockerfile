# Dockerfile

Generate a docker image for our project by running:

```bash
git submodule update --init --recursive
docker build -t skeen/p2p-project .
```
