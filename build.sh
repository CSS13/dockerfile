#!/bin/sh

cd angular
npm i
gulp predeploy
cd ..

docker build -t skeen/streamy:0.0.4 .
docker build -t skeen/streamy:latest .

