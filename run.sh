#!/bin/sh

echo "Starting DHT-fake..."
cd dht-fake && npm start &

echo "Starting angular frontend..."
cd angular && http-server -p 1987
